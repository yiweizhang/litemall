package org.linlinjava.litemall.wx.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 配置文件读取配置
 * @author gxy
 *	2018年6月2日
 *	jsf
 *
 */
@Component
public class WxAuth {
	
	@Value("${wxapp.appId}")
	private String appId;
	
	@Value("${wxapp.secret}")
	private String secret;
	
	@Value("${wxapp.grantType}")
	private String grantType;
	
	@Value("${wxapp.sessionHost}")
	private String sessionHost;
	
	@Value("${wxapp.mudualHost}")
	private String mudualHost;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getSessionHost() {
		return sessionHost;
	}

	public void setSessionHost(String sessionHost) {
		this.sessionHost = sessionHost;
	}

	public String getMudualHost() {
		return mudualHost;
	}

	public void setMudualHost(String mudualHost) {
		this.mudualHost = mudualHost;
	}
	
}
