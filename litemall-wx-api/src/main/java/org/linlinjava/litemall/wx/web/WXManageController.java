package org.linlinjava.litemall.wx.web;

import org.linlinjava.litemall.wx.dao.ModelMessage;
import org.linlinjava.litemall.wx.service.WxService;
import org.linlinjava.litemall.wx.util.AjaxReturnVO;
import org.linlinjava.litemall.wx.util.IsNotNullValit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/wx-manage")
public class WXManageController {
	
	
	private static final Logger log = LoggerFactory.getLogger(WXManageController.class);

	@Autowired
	private WxService wxService;
	
	@PostMapping(value="/getTocken")
	public AjaxReturnVO getTocken(@RequestParam(name="appid")String appid,
			@RequestParam(name="secret")String secret) {
		
		AjaxReturnVO vo =new AjaxReturnVO();
		
		if (!IsNotNullValit.StringIsNotNull(appid) || !IsNotNullValit.StringIsNotNull(secret)) {
			return vo.setFail(vo, "获取accesstocken时参数不能为空");
		}
		
		try {
			vo.setOk(true);
			vo.setData(wxService.getAndSaveTocken(appid,secret));
		} catch (Exception e) {
			vo.setFail(vo, e.getMessage());
			e.printStackTrace();
		}
		
		return vo;
		
	}
	
	@PostMapping(value="/sendMessageModual")
	public AjaxReturnVO sendMessage(@RequestParam(name="touser") String touser,
			@RequestParam(name="template_id") String template_id,
			@RequestParam(name="page") String page,
			@RequestParam(name="form_id") String form_id,
			@RequestParam(name="data") String data,
			@RequestParam(required=false)String color,
			@RequestParam(required=false)String emphasis_keyword,
			@RequestParam(name="tocken")String tocken) {
	
		AjaxReturnVO vo = new AjaxReturnVO();
		
		try {
			
			vo.setOk(true);
			
			ModelMessage mm = new ModelMessage();
			mm.setTouser(touser);
			mm.setTemplate_id(template_id);
			mm.setPage(page);
			mm.setForm_id(form_id);
			mm.setData(data);
			mm.setColor(color);
			mm.setEmphasis_keyword(emphasis_keyword);
			
			String result = wxService.sendModalMessage(mm,tocken);
			
			log.info("模板返回结果："+result);
			
		} catch (Exception e) {
			vo.setFail(vo, e.getMessage());
		}
		
		return vo;
		
	}
	
	@GetMapping(value="/valitModualUrl")
	public String valitModualUrl(@RequestParam(name="signature")String signature,
			@RequestParam(name="timestamp")String timestamp,
			@RequestParam(name="nonce")String nonce,
			@RequestParam(name="echostr")String echostr) {
		
		log.info("start valit signature timestamp="+timestamp);
		
		String result = wxService.ShaSignature(timestamp,nonce);
		
		if(result.equals(result)) {
			return echostr;
		}
		
		return null;
		
	}

}
