package org.linlinjava.litemall.wx.util;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.util.StringUtils;

/**
 * 
 * @author GJY
 *
 * @version 创建时间：2017年11月7日 下午3:11:04 
 *
 * 验证非空
 *
 */
public class IsNotNullValit implements Serializable{

	private static final long serialVersionUID = 5507821424757616576L;
	
	/**
	 * 判断String 是否为空
	 * @param str
	 * @return
	 */
	public static boolean StringIsNotNull(String str) {
		
		if (StringUtils.isEmpty(str) || str.trim().length()==0) {
			
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 判集合是否为空
	 */
	@SuppressWarnings("rawtypes")
	public static boolean CollectionIsNotNull(Collection colls) {
		
		if (colls == null || colls.size() == 0) {
			
			return false;
		}
		
		return true;
		
	}
	
	
	

}
