package org.linlinjava.litemall.wx.dao;

/**
 * 模板消息
 * @author gxy
 *	2018年6月7日
 *	jsf
 *
 */
public class ModelMessage {
	
	private String touser;
	
	private String template_id;
	
	private String page;
	
	private String form_id;
	
	private String data;
	
	private String color;
	
	private String emphasis_keyword;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getForm_id() {
		return form_id;
	}

	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEmphasis_keyword() {
		return emphasis_keyword;
	}

	public void setEmphasis_keyword(String emphasis_keyword) {
		this.emphasis_keyword = emphasis_keyword;
	}

	@Override
	public String toString() {
		return "{touser:" + touser + ", template_id:" + template_id + ", page:" + page + ", form_id:"
				+ form_id + ", data:" + data + ", color:" + color + ", emphasis_keyword:" + emphasis_keyword + "}";
	}
	

}
