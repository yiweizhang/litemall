package org.linlinjava.litemall.wx.util;

import java.io.Serializable;
import java.util.Map;

/**
 * 定义ajax请求返回Json时数据
 * @author gjy
 *
 * 2018年1月30日下午2:45:58
 */
public class AjaxReturnVO implements Serializable {

	private static final long serialVersionUID = 3023474549141299233L;
	
	private String msg;
	
	private Object data;
	
	private boolean ok = true;
	
	private Map<String, Object> attrs;// 其他参数

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public Map<String, Object> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, Object> attrs) {
		this.attrs = attrs;
	}

	
	public AjaxReturnVO setFail(AjaxReturnVO vo,String msg) {
		
		vo.setMsg("失败:"+msg);
		vo.setOk(false);
		
		return vo;
	}
	

}
